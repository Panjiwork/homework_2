package com.itheima.test.UserTest;

import com.itheima.Pojo.User;
import com.itheima.config.MyBatisConfig;
import com.itheima.config.SpringConfig;
import com.itheima.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={SpringConfig.class, MyBatisConfig.class})
public class UserTest {

    @Autowired
    UserService userService;

    @Test
    public void loginTest(){
        User user=new User();
        user.setUsername("张三");
        user.setPassword("123456");
        System.out.println(userService.login(user).toString());
    }
}

package com.itheima.Pojo;

import java.sql.Date;

public class Orders {
    private Integer id;
    private String order_no;
    private Integer order_status;
    private Integer pay_status;
    private Float money;
    private Date create_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public Integer getOrder_status() {
        return order_status;
    }

    public void setOrder_status(Integer order_status) {
        this.order_status = order_status;
    }

    public Integer getPay_status() {
        return pay_status;
    }

    public void setPay_status(Integer pay_status) {
        this.pay_status = pay_status;
    }

    public Float getMoney() {
        return money;
    }

    public void setMoney(Float money) {
        this.money = money;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "id=" + id +
                ", order_no='" + order_no + '\'' +
                ", order_status=" + order_status +
                ", pay_status=" + pay_status +
                ", money=" + money +
                ", create_time=" + create_time +
                '}';
    }
}

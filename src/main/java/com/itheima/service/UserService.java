package com.itheima.service;

import com.itheima.Pojo.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

    User  login(User user);

    User selectAllOrders(Integer id);
}

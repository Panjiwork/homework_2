package com.itheima.service.Impl;

import com.itheima.Pojo.Orders;
import com.itheima.mapper.OrdersMapper;
import com.itheima.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrdersServiceImpl implements OrdersService {
    @Autowired
    private OrdersMapper ordersMapperMapper;

    @Override
    public List<Orders> selectOrdersByUserId(Integer id){return ordersMapperMapper.selectOrdersByUserId(id);}
}

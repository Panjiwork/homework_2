package com.itheima.service;

import com.itheima.Pojo.Orders;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface OrdersService {
    List<Orders> selectOrdersByUserId(Integer id);
}

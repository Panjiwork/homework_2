package com.itheima.mapper;

import com.github.pagehelper.Page;
import com.itheima.Pojo.Orders;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import com.itheima.Pojo.User;

public interface UserMapper{
    @Select("select * from user where name=#{name} and pwd=#{pwd}")
    User login(User user);

    @Select("select * from user where id=#{id} ")
    @Results({
            @Result(id = true,column = "id",property = "id"),
            @Result(column = "name",property = "name"),
            @Result(column = "pwd",property = "pwd"),
            @Result(column = "address",property = "address"),
            @Result(column = "status",property = "status"),
            @Result(column = "register_time",property = "register_time"),
            @Result(column = "login_ip",property = "login_ip"),
            @Result(column = "id" ,property = "ordersList",
                many = @Many(select =
                        "com.itheima.mapper.OrdersMapper.selectOrdersByUserId"
                )
            )
    })
    User selectAllOrders(Integer id);
}
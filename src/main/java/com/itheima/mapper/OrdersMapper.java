package com.itheima.mapper;

import com.itheima.Pojo.Orders;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface OrdersMapper {

    @Select("select * from `order`  where user_id=#{id} ")
    @Results({@Result(id = true,column = "id",property = "id"),
        @Result(column = "order_no",property = "order_no"),
        @Result(column = "order_status",property = "order_status"),
        @Result(column = "pay_status",property = "pay_status"),
        @Result(column = "money",property = "money"),
        @Result(column = "create_time",property = "create_time"),
        @Result(column = "number",property = "number")
    })
    List<Orders> selectOrdersByUserId(Integer id);
}

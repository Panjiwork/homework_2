<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
    <head>
        <meta charset="UTF-8">
        <title>测试页</title>
    </head>
    <body>
        <h2>欢迎你${USER_SESSION.name}!</h2>
        <h3>获取订单</h3>
        <form action="${pageContext.request.contextPath}/user/selectAllOrders" method="post">
            <input type="hidden" name="id" value="${USER_SESSION.id}"/>
            <input type="submit" value="获取本用户的订单"/>
        </form>
            <a  style="text-decoration:none;" href="${pageContext.request.contextPath}/user/logout">
                <span>注销</span>
            </a>

    </body>
</html>
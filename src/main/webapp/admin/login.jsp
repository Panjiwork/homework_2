<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Test</title>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-min.js"></script>
</head>
<body>
    <div>
        <h1>欢迎来到本页面！</h1>
        <h4>用户登录</h4>
        <span style="color: red">${msg}</span>
        <form id="users" action="${pageContext.request.contextPath}/user/login" method="post">
            <span>用户名:</span><input type="text" name="name" id="name"/>
            <span>密&nbsp;码:</span><input type="password" name="pwd" id="pwd"/>
            <input type="submit" value="登录" >
        </form>
    </div>
</body>

</html>